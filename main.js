$(document).ready(function () {
  $("#boton_convertir").click(function () {
    try {
      var result = proyecto.parse($("#textarea_entrada").val());
      $("#textarea_salida").text(result);
      //$("#reproductor").data("xspf_player.swf?playlist_url=testing.xspf");
      //$("#movie").value();
    } catch (e) {
      $("#textarea_salida").html(String(e));
    }
  });
  $("#archivo_entrada").change(cargar);
  $("#boton_guardar").click(function () {
    guardarFichero();
  });
  
});

function cargar(evt) {
  var f = evt.target.files[0];
  var contents = '';
  if (f) {
    var r = new FileReader();
    r.onload = function(e) { 
      contents = e.target.result;
      $("#textarea_entrada").text(contents);
    }
    r.readAsText(f);    
  } else { 
    alert("Error al cargar el archivo.");
  }
}

function guardarFichero() {
    // Comprobar si es Chrome
    if(navigator.userAgent.toLowerCase().indexOf('chrome/') > -1) {
      var textToWrite = document.getElementById("textarea_salida").value;
      var textFileAsBlob = new Blob([textToWrite], {type:'text/plain'});
      var fileNameToSaveAs = "listaReproduccion.xspf";

      var downloadLink = document.createElement("a");
      downloadLink.href = window.webkitURL.createObjectURL(textFileAsBlob);
      downloadLink.download = fileNameToSaveAs;
      downloadLink.click(); 
    } else {
      location.href='data:application/download,' + encodeURIComponent($("#textarea_salida").val());
    }       
}

