%lex

%%

\s+     { /*vacio*/ }
\n      { /* vacio */ }
"{"     { return 'ABR_COR'; }
"}"     { return 'CER_COR'; }
"="     { return 'IGUAL'; }
";"     { return 'PYC'; }

\'(\\.|[^'])*?\'|\"(\\.|[^"])*?\"	{ yytext = yytext.replace(/\"/g,""); return 'VALOR'; }
\b[A-Za-z_/](\w|\/)*\b            { return prOd(yytext); }
<<EOF>>                           { return 'EOF'; }



%%

var palabras_reservadas = {LISTA : 'playlist', NOMBRE : 'title', AUTOR : 'creator', INFO : 'info', CANCION : 'track', TITULO : 'title', ARTISTA : 'creator', URL : 'location', IMAGEN : 'image', ALBUM : 'album', COMENTARIO : 'annotation'};

// Función para determinar si un token es una palabra reservada o un atributo no contemplado.
function prOd(x) {
  return ((x.toUpperCase() in palabras_reservadas) ? x.toUpperCase() : "ATRIBUTO");
}

/lex


%%

//reglas
s : LISTA ABR_COR atr_lista atr_lista_op canciones CER_COR EOF  {
                                                                  var cadena = "";
                                                                  var canciones = "";
                                                                  
                                                                  cadena += ETIQUETA_CABECERA;
                                                                  cadena += ETIQUETA_INICIO;
                                                                  
                                                                  cadena = cadena + $3;
                                                                  for(var i = 0; i < $4.length; i++) {
                                                                    cadena = cadena + $4[i];
                                                                  }
                                                                  for(var i = 0; i < $5.length; i++) {
                                                                    canciones = canciones + $5[i];
                                                                  }
                                                                  canciones = traducirAtributo("trackList",canciones);
                                                                  cadena = cadena + canciones;
                                                                  cadena += ETIQUETA_FINAL;
                                                                  return cadena;
                                                                }
  ;
  
atr_lista : NOMBRE IGUAL VALOR PYC
          {
              $$ = "";
              $$ = (traducirAtributo($1, $3));
          }
          ;
          
atr_lista_op : /* vacio */ { $$ = []; }
             | atr_lista_op AUTOR IGUAL VALOR PYC 
             {
                $$.push(traducirAtributo($2, $4));
             }
             | atr_lista_op INFO IGUAL VALOR PYC 
             {
                $$.push(traducirAtributo($2, $4));
             }
             | atr_lista_op ATRIBUTO IGUAL VALOR PYC
             {
                $$.push(traducirAtributo($2, $4));
             }
             ;   
                   
canciones : /* vacio */ { $$ = []; }
          | canciones cancion 
          {
            $$.push($2);
          }
          ;
          
cancion : CANCION ABR_COR atr_cancion atr_cancion_op CER_COR
        {
          $$ = [];
          
          var temporal = "";
          for (var i = 0; i < $3.length; i++) {
            temporal = temporal + $3[i] + " ";
          }
          for (var i = 0; i < $4.length; i++) {
            temporal = temporal + $4[i] + " ";
          }
          $$.push(traducirAtributo($1, temporal));
        }
        ;

atr_cancion : TITULO IGUAL VALOR PYC ARTISTA IGUAL VALOR PYC URL IGUAL VALOR PYC
                {
                  $$ = [];
                  $$.push(traducirAtributo($1, $3));
                  $$.push(traducirAtributo($5, $7));
                  $$.push(traducirAtributo($9, $11));
                }
            ;
            
atr_cancion_op : /* vacio */ { $$ = []; }
                | atr_cancion_op IMAGEN IGUAL VALOR PYC 
                {
                  $$.push(traducirAtributo($2, $4));
                }
                | atr_cancion_op ALBUM IGUAL VALOR PYC 
                {
                  $$.push(traducirAtributo($2, $4));
                }
                | atr_cancion_op COMENTARIO IGUAL VALOR PYC 
                {
                  $$.push(traducirAtributo($2, $4));
                }
                | atr_cancion_op ATRIBUTO IGUAL VALOR PYC 
                {
                  $$.push(traducirAtributo($2, $4));
                }
                ;         
%%

var ETIQUETA_CABECERA = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
var ETIQUETA_INICIO = "<playlist version=\"0\" xmlns=\"http://xspf.org/ns/0/\">\n";
var ETIQUETA_FINAL = "</playlist>";

var palabras_reservadas = {LISTA : 'playlist', NOMBRE : 'title', AUTOR : 'creator', INFO : 'info', CANCION : 'track', TITULO : 'title', ARTISTA : 'creator', URL : 'location', IMAGEN : 'image', ALBUM : 'album', COMENTARIO : 'annotation'};

function traducirAtributo(atributo, valor) {
  var cadena = '';
  if (atributo.toUpperCase() in palabras_reservadas) {
    cadena = cadena + "<" + palabras_reservadas[atributo.toUpperCase()] + ">" + valor + "</" + palabras_reservadas[atributo.toUpperCase()] + ">\n";
  }
  else {
    cadena = cadena + "<" + atributo + ">" + valor + "</" + atributo + ">\n";
  }
  return cadena;
}
